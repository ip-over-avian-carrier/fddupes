{
  description = "Rust Development Overlay";

  inputs = {
    nixpkgs.url      = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url  = "github:numtide/flake-utils";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, fenix, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ fenix.overlays.default ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };

        toolchain = pkgs.fenix.fromToolchainFile {
          file = ./rust-toolchain.toml;
          sha256 = "sha256-Q/T6uN2OylFuvHL6o4b+T78lW/MYqNHZMbss8DZac0k=";
        };

        buildInputs = with pkgs; [
          openssl
          # rust
          toolchain
        ];

        nativeBuildInputs = with pkgs; [ pkg-config fd ];
        ldLibPath = pkgs.lib.makeLibraryPath buildInputs;
      in
        {
          devShell = pkgs.mkShell {
            buildInputs = buildInputs ++ [ pkgs.rust-analyzer-nightly ];
            nativeBuildInputs = nativeBuildInputs;

            shellHook = ''
            echo "Loaded devshell"
            '';
          };
        }
    );
}
