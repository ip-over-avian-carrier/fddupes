use std::error::Error;
use std::io::{BufWriter, Read, Write};
use std::path::PathBuf;
use std::sync::Arc;
use std::sync::atomic::{AtomicU64, Ordering, AtomicBool};
use std::time::Duration;
use std::{fs, io, thread};
use tokio::sync::mpsc;

use futures::TryStreamExt as _;
use rayon::prelude::*;
use sha2::Digest;
use sha2::Sha256;

static FILES_HASHED: AtomicU64 = AtomicU64::new(0);

fn main() -> Result<(), Box<dyn Error + 'static>> {
    let src_dir = std::env::args().nth(1).ok_or_else(|| {
        help();
        return io::Error::other("missing source dir");
    })?;
    let result_to = std::env::args().nth(2).ok_or_else(|| {
        help();
        return io::Error::other("missing result_to");
    })?;

    println!("hashing all the files in {src_dir} and writing result to {result_to}");

    rayon::ThreadPoolBuilder::new()
        .num_threads(28)
        .build_global()?;

    let done = Arc::new(AtomicBool::new(false));
    let done_cl = Arc::clone(&done);
    let display_handle = thread::spawn(move || display(done_cl));
    let (tx, rx) = mpsc::channel::<Request>(1024);

    let hasher_handle = thread::spawn(move || hasher(rx, result_to));

    let runtime = tokio::runtime::Builder::new_multi_thread()
        .enable_io()
        .worker_threads(2)
        .build()?;

    runtime.block_on(finder(tx, PathBuf::from(src_dir)))?;

    hasher_handle.join().unwrap()?;
    done.store(true, Ordering::Relaxed);
    display_handle.join().unwrap();
    Ok(())
}

fn help() {
    let proc = std::env::args().next().unwrap();
    eprintln!("USAGE: {proc} <folder> <result.txt>");
}

fn display(done: Arc<AtomicBool>) {
    let mut history: [u64; 5] = [0, 0, 0, 0, 0];
    let mut last: u64 = 0;

    let shift = |h: &mut [u64], next: u64| {
        for i in 0..h.len() - 1 {
            h[i] = h[i+1];
        }
        h[h.len() - 1] = next;
    };

    let avg = |h: &[u64]| -> u64 {
        h.iter().copied().sum::<u64>() / h.len() as u64
    };

    while !done.load(Ordering::Relaxed) {
        let current = FILES_HASHED.load(Ordering::Relaxed);
        let delta = current - last;
        last = current;
        shift(&mut history, delta);

        println!("processed {} items ({}/s)", current, avg(&history));
        thread::sleep(Duration::from_secs(1));
    }
}

#[async_recursion::async_recursion]
async fn finder(tx: mpsc::Sender<Request>, sd: PathBuf) -> io::Result<()> {
    let dir = tokio_stream::wrappers::ReadDirStream::new(tokio::fs::read_dir(sd).await?);

    dir.try_for_each_concurrent(Some(8), |item| {
        let ttx = tx.clone();
        async move {
            match item.file_type().await? {
                ft if ft.is_file() => ttx
                    .send(Request { path: item.path() })
                    .await
                    .map_err(io::Error::other)?,
                ft if ft.is_dir() => finder(ttx, item.path()).await?,
                _ => (),
            };

            Ok(())
        }
    })
    .await?;

    Ok(())
}

fn hasher(mut rx: mpsc::Receiver<Request>, result_to: String) -> io::Result<()> {
    let result: Vec<Response> = std::iter::from_fn(move || rx.blocking_recv())
        .par_bridge()
        .map(work)
        .filter_map(|v| v.ok())
        .collect();

    let mut file = BufWriter::new(fs::File::create(result_to)?);

    for response in result {
        for b in response.hash {
            write!(file, "{:02x}", b)?;
        }
        writeln!(file, "  {}", response.path)?
    }

    Ok(())
}

fn work(f: Request) -> io::Result<Response> {
    let mut file = fs::File::open(&f.path)?;
    let mut digest = Sha256::new();

    // Use 16kb buffer, should fit a lot of the files in a single read.
    let mut buf = [0u8; 512 * 1024];

    loop {
        let bread = file.read(&mut buf)?;
        if bread == 0 {
            break;
        }

        digest.update(&buf[..bread]);
    }

    let hash = digest.finalize();

    FILES_HASHED.fetch_add(1, std::sync::atomic::Ordering::Relaxed);

    Ok(Response {
        path: f.path.to_string_lossy().to_string(),
        hash: hash.into(),
    })
}

struct Request {
    path: PathBuf,
}

struct Response {
    path: String,
    hash: [u8; 32],
}
